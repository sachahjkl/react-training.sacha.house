export default function IndexPage() {
  return (
    <article className="p-4">
      <section className="prose mx-auto mt-4 rounded-lg border bg-white p-6 shadow-sm">
        <h2>🚧 React-Training</h2>
        <p>
          This is a demo project using <b>Vite</b>, <b>Typescript</b>,{' '}
          <b>TailwindCSS</b>, <b>React Router V6</b>, <b>Eslint</b> +{' '}
          <b>Prettier</b> and <b>PNPM</b>.
        </p>
        <p>
          I put my <b> demo components</b>, <b>training exercises</b> and
          miscellaneous <b>React webdev stuff</b> on individual pages.
        </p>
      </section>
    </article>
  );
}
