export default function AboutPage() {
  return (
    <article className="p-4">
      <section className="prose mt-4">
        <h2 className="">About</h2>
        <p>
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Repellat,
          minima explicabo necessitatibus mollitia, itaque minus ducimus officia
          odit quaerat adipisci ipsam cum in! Doloribus quo iure necessitatibus
          magnam aperiam neque.
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, neque
          reprehenderit. Sed sequi praesentium labore culpa, deleniti eveniet
          perspiciatis odio non dolorem, rem molestias at tempora laborum
          officia quod veritatis?
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, neque
          reprehenderit. Sed sequi praesentium labore culpa, deleniti eveniet
          perspiciatis odio non dolorem, rem molestias at tempora laborum
          officia quod veritatis?
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, neque
          reprehenderit. Sed sequi praesentium labore culpa, deleniti eveniet
          perspiciatis odio non dolorem, rem molestias at tempora laborum
          officia quod veritatis?
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, neque
          reprehenderit. Sed sequi praesentium labore culpa, deleniti eveniet
          perspiciatis odio non dolorem, rem molestias at tempora laborum
          officia quod veritatis?
        </p>
        <p>
          Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, neque
          reprehenderit. Sed sequi praesentium labore culpa, deleniti eveniet
          perspiciatis odio non dolorem, rem molestias at tempora laborum
          officia quod veritatis?
        </p>
      </section>
    </article>
  );
}
