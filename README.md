# 🚧 React-Training

This is a demo project using **Vite**, **Typescript**, **TailwindCSS**, **React Router V6**, **Eslint** + **Prettier** and **PNPM**.

I put my _demo components_, _training exercises_ and miscellaneous React _webdev stuff_ on individual pages.
